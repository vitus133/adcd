package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	control "gitlab.com/vitus133/adcd/pkg/control"
	measurements "gitlab.com/vitus133/adcd/pkg/measurements"
	metrics "gitlab.com/vitus133/adcd/pkg/metrics"
	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	MeasurementConfig measurements.MeasurementConfig `yaml:"measurementConfig"`
	MetricsConfig     metrics.MetricsConfig          `yaml:"metricsConfig"`
	ControllerConfig  control.ControllerConfig       `yaml:"controllerConfig"`
}

// NewConfig returns a new decoded Config struct
func NewConfig(configPath string) (*Config, error) {
	// Create config structure
	config := &Config{}

	// Open config file
	file, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	// Init new YAML decode
	d := yaml.NewDecoder(file)

	// Start YAML decoding from file
	if err := d.Decode(&config); err != nil {
		return nil, err
	}
	return config, nil
}

// ValidateConfigPath just makes sure, that the path provided is a file,
// that can be read
func ValidateConfigPath(path string) error {
	s, err := os.Stat(path)
	if err != nil {
		return err
	}
	if s.IsDir() {
		return fmt.Errorf("'%s' is a directory, not a normal file", path)
	}
	return nil
}

// ParseFlags will create and parse the CLI flags
// and return the path to be used elsewhere
func ParseFlags() (string, error) {
	// String that contains the configured configuration path
	var configPath string

	// Set up a CLI flag called "-config" to allow users
	// to supply the configuration file
	flag.StringVar(&configPath, "config", "./config.yaml", "path to config file")

	// Actually parse the flags
	flag.Parse()

	// Validate the path first
	if err := ValidateConfigPath(configPath); err != nil {
		return "", err
	}

	// Return the configuration path
	return configPath, nil
}
func main() {
	cfgPath, err := ParseFlags()
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := NewConfig(cfgPath)
	if err != nil {
		log.Fatal(err)
	}

	go cfg.ControllerConfig.Create()
	

	err = measurements.Create(cfg.MeasurementConfig)
	if err != nil {
		log.Fatal(err)
	}

	metrics.StartMetricsServer(cfg.MetricsConfig)
}
