#!/bin/bash

BASE_GPIO_PATH=/sys/class/gpio

right_pump=19
left_pump=26

start_pump(){
    if [ ! -e $BASE_GPIO_PATH/gpio$1 ]; then
        echo "$1" > $BASE_GPIO_PATH/export
    fi
    echo "out" > $BASE_GPIO_PATH/gpio$1/direction
    echo "1" > $BASE_GPIO_PATH/gpio$1/value
}

stop_pump(){
    echo "in" > $BASE_GPIO_PATH/gpio$1/direction
    if [ ! -e $BASE_GPIO_PATH/gpio$1 ]; then
        echo "$1" > $BASE_GPIO_PATH/export
    fi

}
