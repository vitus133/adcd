package adc

import (
	"log"
	"reflect"

	"periph.io/x/conn/v3/analog"
	"periph.io/x/conn/v3/i2c/i2creg"
	"periph.io/x/conn/v3/physic"
	"periph.io/x/devices/v3/ads1x15"
	"periph.io/x/host/v3"
)

const adcPins = 4

func handleUnused(lsb int32) {
}

func Create(updateFreq int64, handlers []func(lsb int32)) error {
	const (
		// updateFrequency = 200000 * physic.MicroHertz
		maxInputVoltage = 5 * physic.Volt
		quality         = ads1x15.BestQuality
	)

	var (
		pins            = make([]ads1x15.PinADC, adcPins)
		channels        = []<-chan analog.Sample{make(<-chan analog.Sample), make(<-chan analog.Sample), make(<-chan analog.Sample), make(<-chan analog.Sample)}
		adcChannels     = []ads1x15.Channel{ads1x15.Channel0, ads1x15.Channel1, ads1x15.Channel2, ads1x15.Channel3}
		updateFrequency = physic.Frequency(updateFreq) * physic.MicroHertz
	)
	for len(handlers) < adcPins {
		handlers = append(handlers, handleUnused)
	}
	// Make sure periph is initialized.
	if _, err := host.Init(); err != nil {
		return err
	}

	// Open default I²C bus.
	bus, err := i2creg.Open("")
	if err != nil {
		return err
	}
	// defer bus.Close()

	// Create a new ADS1115 ADC.
	adc, err := ads1x15.NewADS1115(bus, &ads1x15.DefaultOpts)
	if err != nil {
		return err
	}

	for inp := 0; inp < adcPins; inp++ {
		pins[inp], err = adc.PinForChannel(adcChannels[inp], maxInputVoltage, updateFrequency, quality)
		if err != nil {
			log.Fatalln(err)
		}
		// defer pins[inp].Halt()
		channels[inp] = pins[inp].ReadContinuous()
	}
	cases := make([]reflect.SelectCase, adcPins)
	for i, ch := range channels {
		cases[i] = reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(ch)}
	}
	go func() {
		for {
			chosen, value, ok := reflect.Select(cases)
			if !ok {
				log.Fatal("failed to select cases")
			}
			handlers[chosen](int32(value.Field(1).Int()))
		}
	}()
	return nil
}
