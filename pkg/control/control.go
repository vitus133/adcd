package control

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/vitus133/adcd/pkg/measurements"
)

type ControllerConfig struct {
	// Control loop configuration
	PhTarget                    float64       `yaml:"phTarget"`
	ConductivityTarget          float64       `yaml:"conductivityTarget"`
	ControlPeriod               time.Duration `yaml:"controlPeriod"`
	PhPumpPulseDuration         time.Duration `yaml:"phPumpPulseDuration"`
	FertilizerPumpPulseDuration time.Duration `yaml:"fertilizerPumpPulseDuration"`
	MeasurementValidityTime     time.Duration `yaml:"measurementValidityTime"`
}

func (c *ControllerConfig) Create() {
	t := time.NewTicker(c.ControlPeriod)
	now := time.Now().UTC()
	for {
		<-t.C
		ph, phUpdate := measurements.PhFilter.Get()
		conductivity, condUpdate := measurements.ConductivityFilter.Get()

		if now.Sub(phUpdate) < time.Duration(30*time.Second) && ph > c.PhTarget {
			go log.Print("Pump ph down", ph)
		}

		if now.Sub(condUpdate) < c.MeasurementValidityTime && conductivity < c.ConductivityTarget {
			go log.Print("Pump up the fertilizer", conductivity)
		}
		fmt.Println("tick...")

	}
}
