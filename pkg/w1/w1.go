package w1

import (
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

const w1Path = "/sys/devices/w1_bus_master1/"

func readSensor(id string) (temperature string, err error) {
	sensorPath := filepath.Join(w1Path, id, "temperature")
	dat, err := os.ReadFile(sensorPath)
	if err != nil {
		return "", err
	}
	return strings.Trim(string(dat), "\n"), nil
}

func Create(id string, handler func(temp string)) {
	loop := func() {
		for {
			temp, err := readSensor(id)
			if err != nil {
				log.Default().Print(err)
			}
			handler(temp)
			// TODO: Take period from the config
			time.Sleep(5 * time.Second)
		}

	}
	go loop()
}
