package measurements

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"
)

func TestParseTemperature(t *testing.T) {
	// Parsing success
	want := 27.31
	res, err := parseTemperature("27310")
	if want != res || err != nil {
		t.Fatalf(`parseTemperature("27310") = %v, %v, want match for %v, nil`, res, err, want)
	}
	//Parsing failure
	want = 0.0
	res, err = parseTemperature("443t3")
	if want != res || err == nil {
		t.Fatalf(`parseTemperature("443t3") = %v, %v, want match for %v and error`, res, err, want)
	}

}
func TestHandleTemperature(t *testing.T) {
	temperatureFilter.Create(4) // Average of 4 max
	var buf = new(bytes.Buffer)
	log.SetOutput(buf)
	defer func() {
		log.SetOutput(os.Stdout)
	}()
	HandleTemperature("28000")
	fmt.Println(buf.String())
	want := "28.000"
	res := strings.Split((strings.Split(buf.String(), " ")[3]), "\n")[0]
	if want != res {
		t.Fatalf(`HandleTemperature("28000") = %v, want match for %v`, res, want)
	}

	buf = new(bytes.Buffer)
	log.SetOutput(buf)
	HandleTemperature("26000")
	want = "27.000" // Average of 28 and 26
	fmt.Println("-------")
	fmt.Println(buf.String())
	res = strings.Split((strings.Split(buf.String(), " ")[3]), "\n")[0]
	if want != res {
		t.Fatalf(`HandleTemperature("26000") = %v, want match for %v`, res, want)
	}

	HandleTemperature("25000")
	buf = new(bytes.Buffer)
	log.SetOutput(buf)
	HandleTemperature("24000")
	want = "25.750" // Average of 28, 26, 25 and 24
	fmt.Println("-------")
	fmt.Println(buf.String())
	res = strings.Split((strings.Split(buf.String(), " ")[3]), "\n")[0]
	if want != res {
		t.Fatalf(`HandleTemperature("26000") = %v, want match for %v`, res, want)
	}

}
