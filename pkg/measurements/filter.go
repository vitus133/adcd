package measurements

import (
	"log"
	"time"
)

type Filter struct {
	data       []float64
	taps       uint
	index      uint
	full       bool
	lastUpdate time.Time
}

func (f *Filter) Create(taps uint) {
	f.taps = taps
	f.data = make([]float64, taps)
	f.index = 0
	f.full = false
	log.Printf("created filter with %d taps", f.taps)
}

func sum(array []float64, num uint) float64 {
	var result float64
	for i, v := range array {
		result += v
		if i >= int(num) {
			break
		}
	}
	return result
}

func (f *Filter) Add(data float64) float64 {
	f.lastUpdate = time.Now().UTC()
	f.data[f.index] = data
	f.index += 1
	if f.index >= f.taps {
		f.index = 0
		f.full = true
	}
	if f.full {
		return sum(f.data, f.taps) / float64(f.taps)
	}
	return sum(f.data, f.index) / float64(f.index)
}

func (f *Filter) Get() (float64, time.Time) {
	if f.full {
		return sum(f.data, f.taps) / float64(f.taps), f.lastUpdate
	}
	return sum(f.data, f.index+1) / float64(f.index+1), f.lastUpdate
}
