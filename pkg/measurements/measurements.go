package measurements

import (
	"log"
	"strconv"

	"gitlab.com/vitus133/adcd/pkg/adc"
	"gitlab.com/vitus133/adcd/pkg/metrics"
	"gitlab.com/vitus133/adcd/pkg/w1"
	"gonum.org/v1/gonum/interp"
)

type MeasurementConfig struct {
	// Temperature measurement configuration
	Temperature struct {
		// Temperature measurement periodicity, seconds
		MeasurementPeriodSeconds int `yaml:"measurementPeriod"`
		// Temperature measurement filter depth
		FilterTaps uint `yaml:"filterTaps"`
		// Temperature sensor ID
		SensorId string `yaml:"sensorId"`
	} `yaml:"temperature"`
	// ADC read frequency (PH and conductivity) in micro-Hertz
	AdcReadFrequencyUhz int64 `yaml:"adcReadFrequencyUhz"`
	Conductivity        struct {
		FilterTaps uint      `yaml:"filterTaps"`
		Xs         []float64 `yaml:"xs"`
		Ys         []float64 `yaml:"ys"`
	} `yaml:"conductivity"`
	Ph struct {
		FilterTaps uint      `yaml:"filterTaps"`
		Xs         []float64 `yaml:"xs"`
		Ys         []float64 `yaml:"ys"`
	} `yaml:"ph"`
}

var temperatureFilter Filter
var PhFilter Filter
var ConductivityFilter Filter
var measConfig MeasurementConfig
var phInterpolator interp.PiecewiseLinear
var conductivityInterpolator interp.PiecewiseLinear

func HandlePh(lsb int32) {
	ph := PhFilter.Add(float64(lsb))
	y := phInterpolator.Predict(ph)
	log.Printf("PH handler: %d -> %f", lsb, y)
	metrics.SetPh(y)
	metrics.SetPhRaw(ph)
}

func HandleConductivity(lsb int32) {
	cond := ConductivityFilter.Add(float64(lsb))
	y := conductivityInterpolator.Predict(cond)
	log.Printf("Conductivity handler: %d -> %f", lsb, y)
	metrics.SetConductivity(y)
	metrics.SetConductivityRaw(cond)
}

func parseTemperature(temp string) (float64, error) {
	temperature, err := strconv.ParseFloat(temp, 64)
	if err != nil {
		return 0.0, err
	}
	return temperature / 1000, nil
}

func HandleTemperature(temp string) {
	t, err := parseTemperature(temp)
	if err != nil {
		log.Print(err)
		return
	}
	ft := temperatureFilter.Add(t)
	log.Printf("Temperature: %.3f", ft)
	metrics.SetTemperature(ft)
}

func Create(config MeasurementConfig) error {
	var adcHandlers []func(lsb int32)
	measConfig = config
	adcHandlers = append(adcHandlers, HandlePh)
	adcHandlers = append(adcHandlers, HandleConductivity)
	ConductivityFilter.Create(measConfig.Conductivity.FilterTaps)
	PhFilter.Create(measConfig.Ph.FilterTaps)
	err := phInterpolator.Fit(measConfig.Ph.Xs, measConfig.Ph.Ys)
	if err != nil {
		return err
	}
	err = conductivityInterpolator.Fit(measConfig.Conductivity.Xs, measConfig.Conductivity.Ys)
	if err != nil {
		return err
	}
	temperatureFilter.Create(measConfig.Temperature.FilterTaps)
	w1.Create(measConfig.Temperature.SensorId, HandleTemperature)
	err = adc.Create(measConfig.AdcReadFrequencyUhz, adcHandlers)
	if err != nil {
		return err
	}
	return nil
}
