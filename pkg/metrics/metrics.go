package metrics

import (
	"fmt"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type MetricsConfig struct {
	Node string `yaml:"node"`
	Port int    `yaml:"port"`
}

var metricsConfig MetricsConfig

const (
	Namespace = "hydropo"
	Subsystem = "liquids"
)

var (
	NodeName = ""
	Ph       = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: Namespace,
			Subsystem: Subsystem,
			Name:      "water_ph",
			Help:      "Water PH measurement",
		}, []string{"node"})
	PhRaw = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: Namespace,
			Subsystem: Subsystem,
			Name:      "water_ph_raw",
			Help:      "Water PH ADC reading",
		}, []string{"node"})

	Conductivity = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: Namespace,
			Subsystem: Subsystem,
			Name:      "water_conductivity",
			Help:      "Water conductivity measurement in microsiemens",
		}, []string{"node"})
	ConductivityRaw = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: Namespace,
			Subsystem: Subsystem,
			Name:      "water_conductivity_raw",
			Help:      "Water conductivity ADC reading",
		}, []string{"node"})
	Temperature = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: Namespace,
			Subsystem: Subsystem,
			Name:      "water_temperature",
			Help:      "Water temperature in centigrade",
		}, []string{"node"})
	FertilizerPumpSeconds = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: Namespace,
			Subsystem: Subsystem,
			Name:      "fertilizer_pump_sec",
			Help:      "Counter of seconds fertilizer pump worked",
		}, []string{"node"})
	PhDownPumpSeconds = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: Namespace,
			Subsystem: Subsystem,
			Name:      "ph_down_pump_sec",
			Help:      "Counter of seconds Ph down pump worked",
		}, []string{"node"})
)

func SetPh(ph float64) {
	Ph.With(prometheus.Labels{"node": metricsConfig.Node}).Set(ph)
}

func SetConductivity(conductivity float64) {
	Conductivity.With(prometheus.Labels{"node": metricsConfig.Node}).Set(conductivity)
}

func SetPhRaw(ph float64) {
	PhRaw.With(prometheus.Labels{"node": metricsConfig.Node}).Set(ph)
}

func SetConductivityRaw(conductivity float64) {
	ConductivityRaw.With(prometheus.Labels{"node": metricsConfig.Node}).Set(conductivity)
}

func SetTemperature(temperature float64) {
	Temperature.With(prometheus.Labels{"node": metricsConfig.Node}).Set(temperature)
}

func AddFertilizerPumpSeconds(sec float64) {
	FertilizerPumpSeconds.With(prometheus.Labels{"node": metricsConfig.Node}).Add(sec)
}

func AddPhDownPumpSeconds(sec float64) {
	PhDownPumpSeconds.With(prometheus.Labels{"node": metricsConfig.Node}).Add(sec)
}

// StartMetricsServer runs the prometheus listener so that metrics can be collected
func StartMetricsServer(config MetricsConfig) {
	metricsConfig = config
	reg := prometheus.NewRegistry()
	reg.MustRegister(Ph)
	reg.MustRegister(Conductivity)
	reg.MustRegister(PhRaw)
	reg.MustRegister(ConductivityRaw)
	reg.MustRegister(Temperature)
	reg.MustRegister(FertilizerPumpSeconds)
	reg.MustRegister(PhDownPumpSeconds)
	http.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{
		ErrorLog:            nil,
		ErrorHandling:       0,
		Registry:            reg,
		DisableCompression:  false,
		MaxRequestsInFlight: 0,
		Timeout:             0,
		EnableOpenMetrics:   false,
	}))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", metricsConfig.Port), nil))

}
